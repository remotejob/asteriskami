package config

import (
	"database/sql"
	"fmt"
	"log"

	"github.com/fsnotify/fsnotify"
	"github.com/spf13/viper"
)

type Constants struct {
	PORT string
	Username string
	Secret string
	Host string

	// AccessKey string
	// Source string
	Mysql    struct {
		Dblocation string
		Table      string
	}
	// 	Joenmt struct {
	// 		Url string
	// 	}
	// 	Mlservice struct {
	// 		Url string
	// 	}

	// 	Sqliteml struct {
	// 		Dblocation string
	// 	}
	// 	Accuracy struct {
	// 		Max float64
	// 	}
}

type Config struct {
	Constants
	// Calllog *os.File
	Database   *sql.DB

}

func New() (*Config, error) {
	config := Config{}
	constants, err := initViper()
	config.Constants = constants
	if err != nil {
		return &config, err
	}

	// file, err := os.Open("logs/calls.jsonl")
	// if err != nil {
	// 	log.Fatal(err)
	// }
	// defer file.Close()

	// f, err := os.OpenFile("logs/calls.jsonl",
	// 	os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	// if err != nil {
	// 	// log.Println(err)
	// 	return &config, err
	// }
	// config.Calllog = file

	litedb, err := sql.Open("mysql", config.Mysql.Dblocation)
	if err != nil {
		return &config, err

	}

	config.Database = litedb

	return &config, err
}

func initViper() (Constants, error) {
	viper.SetConfigName("agentstatus.config") // Configuration fileName without the .TOML or .YAML extension
	viper.AddConfigPath(".")                  // Search the root directory for the configuration file
	err := viper.ReadInConfig()               // Find and read the config file
	if err != nil {                           // Handle errors reading the config file
		return Constants{}, err
	}
	viper.WatchConfig() // Watch for changes to the configuration file and recompile
	viper.OnConfigChange(func(e fsnotify.Event) {
		fmt.Println("Config file changed:", e.Name)
	})
	viper.SetDefault("PORT", "8000")
	if err = viper.ReadInConfig(); err != nil {
		log.Panicf("Error reading config file, %s", err)
	}

	var constants Constants
	err = viper.Unmarshal(&constants)
	return constants, err
}
