module gitlab.com/remotejob/asteriskami

go 1.14

require (
	github.com/fsnotify/fsnotify v1.4.9
	github.com/go-chi/chi v4.1.2+incompatible
	github.com/go-sql-driver/mysql v1.5.0
	github.com/heltonmarx/goami v1.0.0
	github.com/magiconair/properties v1.8.2 // indirect
	github.com/mitchellh/mapstructure v1.3.3 // indirect
	github.com/niemeyer/pretty v0.0.0-20200227124842-a10e7caefd8e // indirect
	github.com/pelletier/go-toml v1.8.0 // indirect
	github.com/satori/go.uuid v1.2.0
	github.com/spf13/afero v1.3.5 // indirect
	github.com/spf13/cast v1.3.1 // indirect
	github.com/spf13/jwalterweatherman v1.1.0 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	github.com/spf13/viper v1.7.1
	golang.org/x/sys v0.0.0-20200831180312-196b9ba8737a // indirect
	gopkg.in/check.v1 v1.0.0-20200227125254-8fa46927fb4f // indirect
	gopkg.in/ini.v1 v1.60.2 // indirect
)

replace github.com/heltonmarx/goami => /exwindoz/home/juno/gowork/src/github.com/heltonmarx/goami
