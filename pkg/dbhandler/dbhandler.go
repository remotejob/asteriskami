package dbhandler

import (
	"database/sql"

	"gitlab.com/remotejob/asteriskami/internal/domains"
)

func Getstat(db *sql.DB) (domains.Agentsum, error) {

	var res domains.Agentsum

	err := db.QueryRow("SELECT Loggetin,Available,Lastcall FROM queuesummary limit 1").Scan(&res.Loggetin, &res.Available,&res.Lastcall)
	if err != nil {
		panic(err.Error()) // proper error handling instead of panic in your app
	}

	return res, nil

}
