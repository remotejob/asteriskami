package handler

import (
	"encoding/json"
	"log"
	"net/http"

	"github.com/go-chi/chi"
	"gitlab.com/remotejob/asteriskami/internal/config"
	"gitlab.com/remotejob/asteriskami/pkg/dbhandler"
)

type Config struct {
	*config.Config
}

func New(configuration *config.Config) *Config {
	return &Config{configuration}
}

func (config *Config) Routes() *chi.Mux {
	router := chi.NewRouter()

	router.Get("/agentstat", config.Agentstat)
	// router.Get("/{uuid}/{phone}/{ask}", config.GetAnswer)
	return router
}

func (conf *Config) Agentstat(w http.ResponseWriter, r *http.Request) {

	res, err := dbhandler.Getstat(conf.Database)
	if err != nil {

		panic(err)
	}

	log.Println(res)

	w.Header().Set("Content-Type", "application/json")

	json.NewEncoder(w).Encode(res)

}
