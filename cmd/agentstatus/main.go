package main

import (
	"fmt"
	"log"
	"math"
	"strconv"
	"time"

	"github.com/heltonmarx/goami/ami"
	uuid "github.com/satori/go.uuid"
	"gitlab.com/remotejob/asteriskami/internal/config"

	_ "github.com/go-sql-driver/mysql"
)

var (
	conf           *config.Config
	err            error
	lastcallminago int
)

func init() {

	conf, err = config.New()
	if err != nil {
		log.Panicln("Configuration error", err)
	}

}
func main() {

	defer conf.Database.Close()

	log.Println(conf.Host, conf.Username, conf.Secret)

	socket, err := ami.NewSocket(conf.Host)
	if err != nil {
		log.Fatalf("socket error: %v\n", err)
	}
	if _, err := ami.Connect(socket); err != nil {
		log.Fatalf("connect error: %v\n", err)
	}
	//Login
	auuid, _ := ami.GetUUID()

	err = ami.Login(socket, conf.Username, conf.Secret, "Off", auuid)
	if err != nil {

		log.Panicln(err)
	}

	u1 := uuid.NewV4()
	// fmt.Printf("UUIDv4-1: %s\n", u1)
	// fmt.Printf("login ok!\n")

	res1, err := ami.QueueStatuses(socket, u1.String(), "qlive")
	if err != nil {
		log.Panicln(err)
	}

	// log.Println(res1)
	now := time.Now()

	for _, agent := range res1 {

		log.Println("last call", agent["LastCall"][0])
		lastcallstr := agent["LastCall"][0]

		lastcallint64, err := strconv.ParseInt(lastcallstr, 10, 64)
		if err == nil {

			lastcalltime := time.Now()

			log.Println("lastcallint64", lastcallint64)
			if lastcallint64 != 0 {

				lastcalltime = time.Unix(lastcallint64, 0)

			}

			// lastcalltime := time.Unix(lastcallint64, 0)

			diff := now.Sub(lastcalltime)
			fmt.Println(diff.Minutes())
			mindiff := int(math.Round(diff.Minutes()))
			if mindiff > lastcallminago {
				lastcallminago = mindiff
			}

		}

		log.Println("lastcallminago", lastcallminago)

		// lastcalltime := time.Unix(lastcallint64, 0)
		// fmt.Println(lastcalltime)

	}

	u2 := uuid.NewV4()
	fmt.Printf("UUIDv4-2: %s\n", u2)

	res2, err := ami.QueueSummary(socket, u2.String(), "qlive")
	if err != nil {
		log.Panicln(err)
	}

	log.Println(res2)

	available := res2[0]["Available"][0]

	loggetin := res2[0]["LoggedIn"][0]

	log.Println("available", available, "loggetin", loggetin)

	//Logoff
	fmt.Printf("logoff\n")
	err = ami.Logoff(socket, auuid)
	if err != nil {

		log.Panicln(err)
	}
	stmt, err := conf.Database.Prepare("update queuesummary SET loggetin=?,available=?,lastcall=?")
	if err != nil {

		log.Panicln(err)
	}

	loggetinint, _ := strconv.Atoi(loggetin)
	availableint, _ := strconv.Atoi(available)

	_, err = stmt.Exec(loggetinint, availableint, lastcallminago)

	// results, err := conf.Database.Query("SELECT , name FROM tags")
	// if err != nil {
	//     panic(err.Error()) // proper error handling instead of panic in your app
	// }

	// if err := ami.Logoff(socket, uuid); err != nil {
	// 	log.Fatalf("logoff error: (%v)\n", err)
	// }
	fmt.Printf("goodbye !\n")
}
